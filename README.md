# Power Factor Correction Using Matlab

Probably my first MatLab Program that calculate power factor based upon inputs like voltage, rms frequency etc. It also calculates the capacitor needed to improve the power factor   

## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * MatLab 2008

### How-to-run
 
 * Clone the repo in your local directory
 * Open the file named readme.pdf and follow the detailed instruction from there. (Nearly half a decade ago I had so much time that I described the whole program flow and how to run the program in this pdf in details. I do not think I can make a better readme than that pdf file right now except for may be less grammatical errors ;)  )
 
 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 

